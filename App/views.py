from App.models import Candidate
from django.views.generic import ListView, CreateView, UpdateView,DeleteView
from django.urls import reverse_lazy


#CreaTE
class Create (CreateView):
    model= Candidate
    fields='__all__'
    success_url=reverse_lazy('read')

# Read

class Read (ListView):
    model=Candidate
    queryset=Candidate.objects.all()


class Update(UpdateView):
    model = Candidate
    fields='__all__'
    success_url=reverse_lazy('read')
    

class Delete(DeleteView):
    queryset=Candidate.objects.all()
    success_url=reverse_lazy('read')
    


#Update



#DElete 