
from django.contrib import admin
from django.urls import path
from App import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # 
    path('create/',views.Create.as_view(),name="create"),
    
    path('',views.Read.as_view(),name="read"),
   
    path('update/<int:pk>',views.Update.as_view(),name="update"),
    
    path('delete/<int:pk>',views.Delete.as_view(),name="delete"),
]
